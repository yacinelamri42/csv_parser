#include "sort.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "panic.h"

int compare_2_strings(String a, String b) {
    // assert(a);
    // assert(b);
    if (!(a&&b)) {
	printf("a: %s\nb: %s\n", a, b);
	#ifdef DEBUG
	return -2;
	#endif /* ifdef DEBUG */
	PANIC("NULL");
    }
    size_t length_a = strlen(a);
    size_t length_b = strlen(b);
    size_t max_len = (length_a > length_b) ? length_a : length_b;
    for (int i=0; i<max_len; i++) {
	if (a[i] < b[i]) {
	    return -1;
	}else if (a[i] > b[i]) {
	    return 1;
	}
    }
    return 0;
}

void sort_string_array(String array[], size_t length, int index_arr[]) {
    for (int i=0; i<length; i++) {
	index_arr[i] = i;
    }
    for (int i=0; i<length-1; i++) {
	for (int j=i+1; j<length; j++) {
	    #ifdef DEBUG
	    if (compare_2_strings(array[i], array[j]) == -2) {
		for (int k=0;k<length;k++) {
		    printf("%s\n", array[k]);
		}
		return;
	    }
	    #endif /* ifdef MACRO */
	    if (compare_2_strings(array[i], array[j]) == 1) {
		String temp = array[i];
		array[i] = array[j];
		array[j] = temp;
		int temp_index = index_arr[i];
		index_arr[i] = index_arr[j];
		index_arr[j] = temp_index;
	    }
	}
    }
}
