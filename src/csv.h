#ifndef CSV__H
#define CSV__H

#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>

typedef char* String;

struct CSV {
    size_t num_fields;
    size_t num_entries;
    String **table;
};

typedef struct CSV CSV;

CSV csv_from_file(FILE * file, char entry_delimiter, char field_delimiter);
CSV csv_from_string(String string, char entry_delimiter, char field_delimiter);
CSV csv_from_csv(CSV csv);
bool csv_sort_by(CSV* csv, size_t field);
size_t write_csv_to_file(CSV csv, FILE * file, char entry_delimiter, char field_delimiter);
void print_csv(CSV csv);
void free_csv(CSV csv);

#endif // !CSV__H
