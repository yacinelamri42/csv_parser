#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "panic.h"
#include "csv.h"
#include <stdbool.h>
#include "sort.h"
#define CHUNK 256

size_t write_csv_to_file(CSV csv, FILE * file, char entry_delimiter, char field_delimiter) {
    size_t size = 0;
    for (int i=0; i<csv.num_entries; i++) {
	for (int j=0; j<csv.num_fields; j++) {
	    size+=fprintf(file, "%s%c", csv.table[i][j], field_delimiter);
	}
	size+=fprintf(file, "%c", entry_delimiter);
    }
    return size;
}

CSV csv_from_file(FILE * file, char entry_delimiter, char field_delimiter) {
    fseek(file, 0, SEEK_SET);
    size_t size = 0;
    for (size = 1; (fgetc(file) != EOF); size++);
    fseek(file, 0, SEEK_SET);
    char * str = malloc((size+1)*sizeof(char));
    for (int i=0; (str[i]=fgetc(file))!=EOF && i<size; i++);
    str[size] = 0;
    CSV csv = csv_from_string(str, entry_delimiter, field_delimiter);
    free(str);
    return csv;
}

String **allocate_csv(size_t num_entries, size_t num_fields) {
    String ** table = calloc(num_entries, sizeof(String*));
    if (!table) {
	PANIC("malloc error");
    }
    for (int i=0; i<num_entries; i++) {
	String * entry = calloc(num_fields, sizeof(String));
	if (!entry) {
	    PANIC("malloc error");
	}
	table[i] = entry;
    }
    return table;
}

CSV csv_from_string(String string, char entry_delimiter, char field_delimiter) {
    size_t current_field = 1;
    size_t current_entry = 0;
    size_t num_fields = 1;
    size_t num_entries = 0;
    for (int i=0; string[i]!=0; i++) {
	if (string[i] == entry_delimiter) {
	    current_field = 1;
	    num_entries++;
	}else if (string[i] == field_delimiter) {
	    current_field++;
	    if (current_field>num_fields) {
		num_fields = current_field;
	    }
	}
    }
    String **table = allocate_csv(num_entries, num_fields);

    current_entry = 0;
    current_field = 0;
    String buffer = calloc(CHUNK,sizeof(char));
    size_t buffer_alloc_size = CHUNK;
    size_t buffer_size = 1;
    for (int i=0; string[i]!=0; i++) {
	if (string[i] == entry_delimiter) {
	    String new_string = malloc((buffer_size)*sizeof(char));
	    if (!new_string) {
		PANIC("malloc error");
	    }
	    memcpy(new_string, buffer, buffer_size);
	    new_string[buffer_size-1] = 0;
	    table[current_entry++][current_field] = new_string;
	    current_field = 0;
	    buffer_size = 1;
	}else if (string[i] == field_delimiter) {
	    String new_string = malloc((buffer_size)*sizeof(char));
	    if (!new_string) {
		PANIC("malloc error");
	    }
	    memcpy(new_string, buffer, buffer_size);
	    new_string[buffer_size-1] = 0;
	    table[current_entry][current_field++] = new_string;
	    buffer_size = 1;
	}else {
	    if (buffer_size == buffer_alloc_size) {
		buffer_alloc_size+=CHUNK;
		String buf = realloc(buffer, buffer_alloc_size);
		if (!buf) {
		    PANIC("realloc error");
		}
		buffer = buf;
	    }
	    buffer[buffer_size++-1] = string[i];
	}
    }
    free(buffer);
    return (CSV) {
	.table = table,
	.num_entries = num_entries,
	.num_fields = num_fields,
    };
}

void print_csv(CSV csv) {
    for (int i=0; i<csv.num_entries; i++) {
	for (int j=0; j<csv.num_fields; j++) {
	    printf("%s ", csv.table[i][j]);
	}
	printf("\n");
    }
}

String * get_entry(CSV csv, size_t entry_num) {
    return (entry_num < csv.num_entries) ? csv.table[entry_num] : NULL;
}

CSV csv_from_csv(CSV csv) {
    String ** table = allocate_csv(csv.num_entries, csv.num_fields);
    for (int i=0; i<csv.num_entries; i++) {
	for (int j=0; j<csv.num_fields; j++) {
	    size_t length = strlen(csv.table[i][j]);
	    String buf = malloc(sizeof(char)*length);
	    if (!buf) {
		PANIC("malloc error");
	    }
	    memcpy(buf, csv.table[i][j], length);
	    table[i][j] = buf;
	}
    }
    return (CSV) {
	.table = table,
	.num_fields = csv.num_fields,
	.num_entries = csv.num_entries,
    };
}

bool csv_sort_by(CSV* csv, size_t field) {
    if (field >= csv->num_fields) {
	return false;
    }
    String * temp_array = calloc(csv->num_entries-1, sizeof(String));
    int * temp_indexes = calloc(csv->num_entries-1, sizeof(int));
    for (int i=1; i<csv->num_entries; i++) {
	temp_array[i-1] = csv->table[i][field];
    }
    sort_string_array(temp_array, csv->num_entries-1, temp_indexes);
    free(temp_array);
    String **table = calloc(csv->num_entries, sizeof(String*));
    table[0] = csv->table[0];
    for (int i=1; i<csv->num_entries; i++) {
	table[i] = csv->table[temp_indexes[i-1]+1];
    }
    free(csv->table);
    csv->table = table;
    free(temp_indexes);
    return true;
}

void free_csv(CSV csv) {
    for (int i=0; i<csv.num_entries; i++) {
	for (int j=0; j<csv.num_fields; j++) {
	    free(csv.table[i][j]);
	}
	free(csv.table[i]);
    }
    free(csv.table);
}

