#ifndef PANIC__H

#define PANIC(err_message) panic(err_message, __LINE__, __FUNCTION__)
_Noreturn void panic(char * err_message, int line_number, const char* function_name);

#endif 
