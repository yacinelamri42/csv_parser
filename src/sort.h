#ifndef SORT__H
#define SORT__H

typedef char* String;
#include <stddef.h>

void sort_string_array(String array[], size_t length, int index_arr[]);

#endif // !SORT__H
