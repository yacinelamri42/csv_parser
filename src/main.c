#include <stdio.h>
#include "panic.h"
#include "csv.h"


int main(int argc, char *argv[]){
    if (argc != 2) {
	printf("Usage: %s [csv_file]\n", argv[0]);
	return 1;
    }
    FILE * file = fopen(argv[1], "r");
    if (!file) {
	PANIC("could not open file");
    }
    CSV csv = csv_from_file(file, '\n', ',');
    fclose(file);
    for (int i=0; i<csv.num_fields; i++) {
	char number[256];
	sprintf(number, "%d.csv", i);
	FILE * file1 = fopen(number, "w");
	if (!file1) {
	    PANIC("could not open file");
	}

	if (!csv_sort_by(&csv, i)) {
	    PANIC("sort failed");
	}
	size_t size = write_csv_to_file(csv, file1, '\n', ',');
	printf("written %ld characters\n", size);
	fclose(file1);
    }
    free_csv(csv);
    return 0;
}
