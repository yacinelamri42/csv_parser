CC := gcc
LD := gcc
CFLAGS := -Wall -std=c99 -g
LDFLAGS := -fsanitize=address -L./libs/
LIBS := -lc -lpanic

APPNAME := csv_parser

BUILD := ./build/
SRC := ./src/

CSRC := $(wildcard $(SRC)/*.c)
COBJ := $(CSRC:.c=.o)

.PHONY: clean

$(BUILD)/$(APPNAME): $(COBJ)
	$(LD) $(LDFLAGS) $^ -o $@ $(LIBS)

%.o: %.c
	$(CC) $(CFLAGS) -c $^ -o $@

clean:
	-rm -rf $(BUILD)/*
	-find $(SRC) -name *.o -delete
